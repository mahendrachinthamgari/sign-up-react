import React, { Component } from "react";
import validator from "validator";
import "./SignUp.css";

class SignUp extends Component {
    constructor() {
        super();
        this.state = {
            firstName: "",
            lastName: "",
            age: "",
            gender: "",
            eMail: "",
            phoneNumber: "",
            password: "",
            repeatPassword: "",
            role: "",
            termsConditiions: false,
            success: "",
            error: {
                eFName: "",
                eLName: "",
                eAge: "",
                eGender: "",
                eEmail: "",
                ePhoneNumber: "",
                ePassword: "",
                eRepeatPassword: "",
                eTermsConditions: "",
                eRole: "",
                eSuccess: ""

            }
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let errors = {};
        let count = 0;

        if (!validator.isAlpha((this.state.firstName).trim())) {
            errors.eFName = "Can't be empty or only contains Alphabets";
        } else {
            count++;
        }

        if (!validator.isAlpha((this.state.lastName).trim())) {
            errors.eLName = "Can't be empty or only contains Alphabets";
        } else {
            count++;
        }

        if (!validator.isInt(this.state.age) || this.state.age.length === 0 || this.state.age < 1) {
            errors.eAge = "Invalid Age, Please type only Positive Numbers";
        } else {
            count++;
        }
        if (this.state.gender === "") {
            errors.eGender = "Please select Gender";
        } else {
            count++;
        }
        if (!validator.isEmail(this.state.eMail)) {
            errors.eEmail = "Invalid Email";
        } else {
            count++;
        }
        if (this.state.role === "1" || this.state.role === "") {
            errors.eRole = "Please select Role";
        } else {
            count++;
        }
        if (!validator.isStrongPassword(this.state.password)) {
            let msg = ["Must contains 1 upper case", "Must contains 1 lower case", "Must contains 1 number", "Must contains 1 symbol"];
            let msgValues = (this.state.password).split("").map((char) => {
                if (isNaN(char)) {
                    if (!validator.isAlphanumeric(char)) {
                        msg.splice(3, 1, "")
                        return 5;
                    } else if (validator.isLowercase(char)) {
                        msg.splice(1, 1, "")
                        return 3;
                    } else if (validator.isUppercase(char)) {
                        msg.splice(0, 1, "");
                        return 2;
                    } else {
                        return 6;
                    }
                } else {
                    msg.splice(2, 1, "");
                    return -1;
                }
            });
            if (msgValues.length < 8) {
                msg.push("Length must be greater than 7")
            }
            errors.ePassword = msg.join(" ");

        } else {
            count++;
        }
        if (this.state.password !== this.state.repeatPassword) {
            errors.eRepeatPassword = "Password Mismatched";
        } else {
            count++;
        }
        if (this.state.termsConditiions === false) {
            errors.eTermsConditions = "Please click on the to agree our terms and conditions";
        } else {
            count++;
        }
        if (count === 9) {
            errors.eSuccess = "Success";
        }

        this.setState({ error: errors });
    }

    render() {
        return (
            <section className="min-vh-100 gradient-custom">
                <div className="container py-5">
                    <div className="row justify-content-center align-items-center h-100">
                        <div className="col-12 col-lg-9 col-xl-7">
                            <div className="card shadow-2-strong card-registration">
                                <div className="card-body p-4 p-md-5">
                                    <h3 className="h2 mb-4 pb-2 pb-md-0 mb-md-5">Registration Form</h3>
                                    <form onSubmit={this.handleSubmit}>

                                        <div className="row">
                                            <div className="col-md-6 mb-4">

                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="firstName"> <i className="fa-solid fa-user"></i> First Name </label>
                                                    <input type="text" id="firstName" className="form-control form-control-lg" placeholder="First Name" onChange={(event) => {
                                                        this.setState({ firstName: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.eFName}</small>
                                                </div>

                                            </div>
                                            <div className="col-md-6 mb-4">

                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="lastName"> <i className="fa-solid fa-user"></i> Last Name</label>
                                                    <input type="text" id="lastName" className="form-control form-control-lg" placeholder="Last Name" onChange={(event) => {
                                                        this.setState({ lastName: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.eLName}</small>
                                                </div>

                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-6 mb-4 d-flex align-items-center">

                                                <div className="form-outline datepicker w-100">
                                                    <label htmlFor="age" className="h5 form-label"> <i className="fa-solid fa-person-cane"></i> Age</label>
                                                    <input type="text" className="form-control form-control-lg" id="age" placeholder="Age" onChange={(event) => {
                                                        this.setState({ age: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.eAge}</small>
                                                </div>

                                            </div>
                                            <div className="col-md-6 mb-4">

                                                <h5 className="mb-2 pb-1 h5"><i className="fa-solid fa-genderless"></i> Gender: </h5>

                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="femaleGender"
                                                        value="option1" checked={this.state.gender === "option1"} onChange={(event) => {
                                                            this.setState({ gender: event.target.value });
                                                        }} />
                                                    <label className="form-check-label" htmlFor="femaleGender">Female</label>
                                                </div>

                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="maleGender"
                                                        value="option2" checked={this.state.gender === "option2"} onChange={(event) => {
                                                            this.setState({ gender: event.target.value });
                                                        }} />
                                                    <label className="form-check-label" htmlFor="maleGender">Male</label>
                                                </div>

                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="otherGender"
                                                        value="option3" checked={this.state.gender === "option3"} onChange={(event) => {
                                                            this.setState({ gender: event.target.value });
                                                        }} />
                                                    <label className="form-check-label" htmlFor="otherGender">Other</label>
                                                </div>
                                                <div>
                                                    <small>{this.state.error.eGender}</small>
                                                </div>

                                            </div>

                                        </div>

                                        <div className="row">
                                            <div className="col-md-6 mb-4 pb-2">

                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="emailAddress"> <i className="fa-solid fa-envelope"></i> Email</label>
                                                    <input type="email" id="emailAddress" className="form-control form-control-lg" placeholder="Email" onChange={(event) => {
                                                        this.setState({ eMail: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.eEmail}</small>
                                                </div>

                                            </div>
                                            <div className="col-md-6 mb-4 pb-2">


                                                <div className="d-flex flex-column">
                                                    <label className="h5 form-label select-label">Please Select Role</label>
                                                    <select className="select form-control-lg drop-down">
                                                        <option value="1" onClick={(event) => {
                                                            this.setState({ role: event.target.value })
                                                        }}>Select Role</option>
                                                        <option value="2" onClick={(event) => {
                                                            this.setState({ role: event.target.value })
                                                        }}>Developer</option>
                                                        <option value="3" onClick={(event) => {
                                                            this.setState({ role: event.target.value })
                                                        }}>Senior Developer</option>
                                                        <option value="4" onClick={(event) => {
                                                            this.setState({ role: event.target.value })
                                                        }}>Lead Engineer</option>
                                                        <option value="5" onClick={(event) => {
                                                            this.setState({ role: event.target.value })
                                                        }}>CTO</option>
                                                    </select>

                                                    <small>{this.state.error.eRole}</small>

                                                </div>

                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 mb-4 pb-2">

                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="password"><i className="fa-solid fa-lock"></i> Password</label>
                                                    <input type="password" id="password" className="form-control form-control-lg" placeholder="New Password" onChange={(event) => {
                                                        this.setState({ password: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.ePassword}</small>
                                                </div>

                                            </div>

                                            <div className="col-md-6 mb-4 pb-2">
                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="repeat-password"><i className="fa-solid fa-lock"></i> Repeat Password</label>
                                                    <input type="password" id="repeat-password" className="form-control form-control-lg" placeholder="Re-enter Password" onChange={(event) => {
                                                        this.setState({ repeatPassword: event.target.value });
                                                        console.log(event.target.value)
                                                    }} />
                                                    <small>{this.state.error.eRepeatPassword}</small>
                                                </div>

                                            </div>

                                        </div>
                                        <div className="row bottom-container">
                                            <div className="col-6">
                                                <div className="form-check d-flex justify-content-center mt-3">
                                                    <div className="d-flex justify-content-end">
                                                        <input className="form-check-input me-2" type="checkbox" id="check-box" onClick={(event) => {
                                                            this.setState({ termsConditiions: event.target.checked });
                                                        }} />
                                                        <label className="form-check-label" htmlFor="check-box">
                                                            Agree to <a href="#!" className="text-body"><u>terms and conditions</u></a>
                                                        </label>
                                                    </div>
                                                </div>
                                                <small>{this.state.error.eTermsConditions}</small>
                                            </div>
                                            <div className="col-6">
                                                <div className="pt-2">
                                                    <input className="btn btn-primary btn-md" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                        <h1>{this.state.error.eSuccess}</h1>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

        );
    }
}

export default SignUp;